# Semantic Highlights

Colorizes each variable with their individual color.

Compatible with python and pylance.

## Features

Before:

![before](https://gitlab.com/MalcolmMielle/semantic-highlighting/-/raw/main/images/before.png)

After:

![after](https://gitlab.com/MalcolmMielle/semantic-highlighting/-/raw/main/images/after.png)


## How to contribute

If you're on GitHub, head to the GitLab repo of this extension and submit Issues or PR.
Help is always welcomed!

## Release Notes

First release :).

<!-- ----------------------------------------------------------------------------------------------------------- -->
<!--
## Working with Markdown

**Note:** You can author your README using Visual Studio Code.  Here are some useful editor keyboard shortcuts:

* Split the editor (`Cmd+\` on macOS or `Ctrl+\` on Windows and Linux)
* Toggle preview (`Shift+CMD+V` on macOS or `Shift+Ctrl+V` on Windows and Linux)
* Press `Ctrl+Space` (Windows, Linux) or `Cmd+Space` (macOS) to see a list of Markdown snippets

### For more information

* [Visual Studio Code's Markdown Support](http://code.visualstudio.com/docs/languages/markdown)
* [Markdown Syntax Reference](https://help.github.com/articles/markdown-basics/)

**Enjoy!** -->

Icon from [Freepik](https://www.freepik.com/)
